package com.example.practica3c1kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.practica3c1kotlin.ui.theme.Practica3c1KotlinTheme

class CalculadoraActivity : AppCompatActivity() {
    //Declaración de variables de tipo boton
    private lateinit var btnSumar: Button   //Posteriormente se puede inicializar y solo una referencia
    private lateinit var btnRestar: Button
    private lateinit var btnMultiplicar: Button
    private lateinit var btnDividir: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button

    //Declaración de variables tipo EditText
    private lateinit var txtNum1: EditText
    private lateinit var txtNum2: EditText

    //Declaracion de variable tipo TextView
    private lateinit var lblResultado: TextView

    //Construir la clase Calculadora
    private var calculadora = Calculadora(0.0F, 0.0F)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora)
        iniciarComponentes()

        //Funciones al dar click a los botones
        btnSumar.setOnClickListener{
            btnSumar()
        }

        btnRestar.setOnClickListener{
            btnRestar()
        }

        btnMultiplicar.setOnClickListener{
            btnMultiplicar()
        }

        btnDividir.setOnClickListener{
            btnDividir()
        }

        btnRegresar.setOnClickListener{
            regresar()
        }

        btnLimpiar.setOnClickListener{
            limpiar()
        }
    }

    //Funcion para inicial los componentes y hacer la relacion entre vista y controlador
    private fun iniciarComponentes(){
        btnSumar = findViewById(R.id.btnSumar)
        btnRestar = findViewById(R.id.btnRestar)
        btnMultiplicar = findViewById(R.id.btnMultiplicar)
        btnDividir = findViewById(R.id.btnDividir)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)

        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)

        lblResultado = findViewById(R.id.lblResultado)
    }

    //Funciones de los botones
    private fun btnSumar(){
        calculadora.num1 = txtNum1.text.toString().toFloat()
        calculadora.num2 = txtNum2.text.toString().toFloat()
        lblResultado.text = calculadora.sumar().toString()
    }

    private fun btnRestar(){
        calculadora.num1 = txtNum1.text.toString().toFloat()
        calculadora.num2 = txtNum2.text.toString().toFloat()
        lblResultado.text = calculadora.restar().toString()
    }

    private fun btnMultiplicar(){
        calculadora.num1 = txtNum1.text.toString().toFloat()
        calculadora.num2 = txtNum2.text.toString().toFloat()
        lblResultado.text = calculadora.multiplicar().toString()
    }

    private fun btnDividir(){
        calculadora.num1 = txtNum1.text.toString().toFloat()
        calculadora.num2 = txtNum2.text.toString().toFloat()
        lblResultado.text = calculadora.dividir().toString()
    }

    //Función para limpiar la pantalla
    private fun limpiar(){
        txtNum1.setText("")
        txtNum2.setText("")
        txtNum1.requestFocus()
        txtNum2.requestFocus()
        lblResultado.setText("")
    }

    //Función para regresar, donde se muestra un mensaje para dar una segunda confirmacion
    private fun regresar(){
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora")
        confirmar.setMessage(" ¿Desea regresar? ")
        confirmar.setPositiveButton("Confirmar"){
            dialogInterface,which->finish()
        }
        confirmar.setNegativeButton("Cancelar"){
            dialogInterface, which->}.show()
    }
}











