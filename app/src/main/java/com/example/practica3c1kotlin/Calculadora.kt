package com.example.practica3c1kotlin

class Calculadora {
    //Declaracion de variables
    var num1:Float = 0.0F
    var num2:Float = 0.0F

    //Constructor
    constructor(num1:Float, num2:Float){
        this.num1 = num1
        this.num2 = num2
    }

    //Funciones de la clase
    fun sumar():Float{
        return num1 + num2
    }

    fun restar():Float{
        return num1 - num2
    }

    fun multiplicar():Float{
        return num1 * num2
    }

    fun dividir():Float{
        var total = 0.0F

        if (num2 > 0.0F){
            return num1 / num2
        }

        return total
    }
}