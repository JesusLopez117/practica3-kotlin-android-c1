package com.example.practica3c1kotlin

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.practica3c1kotlin.ui.theme.Practica3c1KotlinTheme

class MainActivity : ComponentActivity() {
    //Declaracion de variables tipo boton
    private lateinit var btnIngresar: Button
    private lateinit var btnCerrar: Button

    //Declaracion de variables tipo EditText
    private lateinit var txtUsuario: EditText
    private lateinit var txtPassword: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnIngresar.setOnClickListener(){
            this.ingresar()
        }

        btnCerrar.setOnClickListener(){
            finish()
        }

    }

    //Funcion para iniciar los componentes
    private fun iniciarComponentes() {
        btnIngresar = findViewById(R.id.btnIngresar)
        btnCerrar = findViewById(R.id.btnCerrar)
        txtUsuario = findViewById(R.id.txtUsuario)
        txtPassword = findViewById(R.id.txtPassword)
    }

    //Funcion para ingresar
    private fun ingresar(){
        //Declaración de variables
        var strUser: String
        var strPassword: String

        //Asignar los String que se declararon en R.Value
        strUser = application.resources.getString(R.string.user)
        strPassword = application.resources.getString(R.string.password)

        //Validación del usuarios y contraseña
        if (txtUsuario.text.toString().equals(strUser) && txtPassword.text.toString().equals(strPassword)) {

            //Hacer paquete de datos que se enviaran
            //Siempre se crear un objeto Bundle() = Paquete
            var bundle = Bundle()
            bundle.putString("usuario", strUser)

            val intent = Intent(this@MainActivity, CalculadoraActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
            txtUsuario.setText("")
            txtPassword.setText("")
        } else {
            Toast.makeText(
                this.applicationContext, "User o Password no validos",
                Toast.LENGTH_LONG
            ).show()
        }
    }

}
